<?php
class FormView
{
    private $template;
    private $message;

    public function __construct(string $message)
    {
        $this->template = View::init();
        $this->message = $message;
    }
    public function getRegistrationForm()
    {
        $this->template->assign('auth', '');
        if ($this->message != 'OK')
            $this->template->assign('content', self::Signup($this->template, $this->message));
        else $this->template->assign('content', 'Вы успешно зарегистрированы');
        $this->template->assign('pagination', '');
        $this->template->display('index.tpl');
    }
    //только для неавторизованного пользователя или при входе с ошибкой
    public static function Auth(Smarty &$template, string $errorMessage = "")
    {
        $template->assign("auth_error", $errorMessage);
        unset($_SESSION['auth_error']);
        return $template->fetch('header_form_auth.tpl');
    }
    //если перешел на страницу регистрации
    public static function Signup(Smarty &$template, string $errorMessage = "")
    {
        $template->assign("signup_error", $errorMessage);
        return $template->fetch('form.tpl');
    }
    public static function UserInfo(&$template)
    {
        $template->assign("name", $_SESSION['name']);
        $template->assign('email', $_SESSION['mail']);
        return $template->fetch('header_user_block.tpl');
    }
    public function render()
    {
        $this->getRegistrationForm();
    }
}
