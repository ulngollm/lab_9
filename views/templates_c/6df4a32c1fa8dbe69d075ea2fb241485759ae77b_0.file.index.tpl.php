<?php
/* Smarty version 3.1.36, created on 2020-12-20 00:38:50
  from '/home/ully/sites/localhost/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5fde72eabc71a2_76079222',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6df4a32c1fa8dbe69d075ea2fb241485759ae77b' => 
    array (
      0 => '/home/ully/sites/localhost/templates/index.tpl',
      1 => 1608413102,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header_form_auth.tpl' => 1,
  ),
),false)) {
function content_5fde72eabc71a2_76079222 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>Лабораторная работа</title>
    <link rel="stylesheet" type="text/css" href="/views/css/style.css">
</head>

<body>
    <div id="wrapper">
        <div id="header">
            <div class="header__title">Лабораторная работа</div>
            <div class="header__user-block">
                <?php $_smarty_tpl->_subTemplateRender("file:header_form_auth.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            </div>
        </div>
        <div id="menu">
            <?php echo $_smarty_tpl->tpl_vars['menu']->value;?>

        </div>
        <div id="content">
            <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

            <div id="pagination">
                <?php echo $_smarty_tpl->tpl_vars['pagination']->value;?>

            </div>
        </div>

        <div id="footer">Донской Государственный Технический Университет <br> ully</div>
    </div>
</body>

</html><?php }
}
