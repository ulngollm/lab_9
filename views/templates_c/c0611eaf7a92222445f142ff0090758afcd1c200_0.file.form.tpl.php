<?php
/* Smarty version 3.1.36, created on 2020-12-20 01:17:34
  from '/home/ully/sites/localhost/views/templates/form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5fde7bfe040162_57182189',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c0611eaf7a92222445f142ff0090758afcd1c200' => 
    array (
      0 => '/home/ully/sites/localhost/views/templates/form.tpl',
      1 => 1608416252,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fde7bfe040162_57182189 (Smarty_Internal_Template $_smarty_tpl) {
?><form name="signup" action="/signup" method="POST">
    <label class="form__input">Имя<input name="name" type="text" required></label>
    <label class="form__input">Email<input name="mail" type="email" required></label>
    <label class="form__input">Пароль<input name="password" type="password" required></label>
    <label class="form__input">Подтверждение пароля<input name="password_confirm" type="password" required></label>
    <output name="error" class="error_message"><?php echo $_smarty_tpl->tpl_vars['signup_error']->value;?>
</output>
    <input class="form__input link button" name="signup" type="submit" value="Зарегистрироваться">
</form>
<?php echo '<script'; ?>
>
    document.forms.signup.addEventListener('input', (e)=>e.currentTarget.error.innerText = '');
    document.forms.signup.addEventListener('submit', (e) => {
        if (e.target.password.value != e.target.password_confirm.value) {
            e.preventDefault();
            e.target.error.innerText = 'Пароли не совпадают!';
        }
    });
<?php echo '</script'; ?>
><?php }
}
