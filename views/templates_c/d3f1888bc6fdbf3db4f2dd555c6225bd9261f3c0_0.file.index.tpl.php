<?php
/* Smarty version 3.1.36, created on 2020-12-20 01:19:03
  from '/home/ully/sites/localhost/views/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5fde7c574aa836_83052248',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd3f1888bc6fdbf3db4f2dd555c6225bd9261f3c0' => 
    array (
      0 => '/home/ully/sites/localhost/views/templates/index.tpl',
      1 => 1608416316,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fde7c574aa836_83052248 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>Лабораторная работа</title>
    <link rel="stylesheet" type="text/css" href="/views/css/style.css">
</head>

<body>
    <div id="wrapper">
        <div id="header">
            <div class="header__title">Лабораторная работа</div>
            <div class="header__user-block">
                <?php echo $_smarty_tpl->tpl_vars['auth']->value;?>

            </div>
        </div>
        <div id="menu">
            <?php echo $_smarty_tpl->tpl_vars['menu']->value;?>

        </div>
        <div id="content">
            <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

            <div id="pagination">
                <?php echo $_smarty_tpl->tpl_vars['pagination']->value;?>

            </div>
        </div>

        <div id="footer">Донской Государственный Технический Университет <br> ully</div>
    </div>
</body>

</html><?php }
}
