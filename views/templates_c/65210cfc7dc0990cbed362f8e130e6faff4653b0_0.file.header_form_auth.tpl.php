<?php
/* Smarty version 3.1.36, created on 2020-12-20 00:38:50
  from '/home/ully/sites/localhost/templates/header_form_auth.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.36',
  'unifunc' => 'content_5fde72eabd1185_65223798',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '65210cfc7dc0990cbed362f8e130e6faff4653b0' => 
    array (
      0 => '/home/ully/sites/localhost/templates/header_form_auth.tpl',
      1 => 1608413065,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5fde72eabd1185_65223798 (Smarty_Internal_Template $_smarty_tpl) {
?><form name="auth" class="header__form" action="/auth" method="post">
    <label class="form__input">
        <input name="mail" type="text" required>
    </label>
    <label class="form__input">
        <input name="password" type="password" required>
    </label>
    <input class="form__input button link" name="auth" type="submit" value="Вход">
    <a class="button link" href="/signup">Регистрация</a>
</form>
<span class="error_message"><?php echo $_smarty_tpl->tpl_vars['auth_error']->value;?>
</span>
<?php echo '<script'; ?>
>
    document.forms.auth.addEventListener('input', ()=>document.querySelector('.error_message').innerText = '');
<?php echo '</script'; ?>
><?php }
}
