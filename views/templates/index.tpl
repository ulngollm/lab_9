<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>Лабораторная работа</title>
    <link rel="stylesheet" type="text/css" href="/views/css/style.css">
</head>

<body>
    <div id="wrapper">
        <div id="header">
            <div class="header__title">Лабораторная работа</div>
            <div class="header__user-block">
                {$auth}
            </div>
        </div>
        <div id="menu">
            {$menu}
        </div>
        <div id="content">
            {$content}
            <div id="pagination">
                {$pagination}
            </div>
        </div>

        <div id="footer">Донской Государственный Технический Университет <br> ully</div>
    </div>
</body>

</html>