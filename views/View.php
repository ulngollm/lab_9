<?php
require_once ROOT. '/components/libs/Smarty.class.php';


class View
{
    public static function init($currentSection = '/', $isAuth = false, string $authError = "")
    {
        if(!empty($_SESSION)){
            (empty($_SESSION['auth_error']))? $isAuth = true: $authError = $_SESSION['auth_error'];
        }
        $template = new Smarty(); 
        $template->setTemplateDir(ROOT.'/views/templates');
        $template->setCompileDir(ROOT.'/views/templates_c');
        $menu = new Menu($currentSection);
        if ($isAuth) {
            $auth = FormView::UserInfo($template);
        } else $auth = FormView::Auth($template, $authError);

        $template->assign('menu', $menu->init());
        $template->assign('auth', $auth);
        return $template;
    }
}
