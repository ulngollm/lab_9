<?php
class ContentView
{
    private $data;
    private $viewType; //list or detail
    private $template;
    private $pagination;
    private $section;

    public function __construct(array $sqlResult, string $viewType, string $section, int $currentPage = 0, int $rowsCount = 0 )
    {
        $this->data = $sqlResult;
        $this->viewType = $viewType;
        $this->section = $section;
        $this->rowsCount = $rowsCount;
        $this->template = View::init($section);
        $baseLink = "/$section/list";
        if($viewType == "list") $this->pagination = new Pagination($rowsCount, $currentPage, $baseLink);
    }
    public static function FormatResult(array $sqlResult, $section = '', $isList = true)
    {
        $flagfirst = true;

        $result = "<table><thead>";
        foreach($sqlResult as $row) {
            if ($flagfirst) {
                $key = array_keys($row);
                $result .= "<tr>";
                for ($i = 0; $i < count($key); $i++) {
                    $result .= "<th>$key[$i]</th>";
                }
                if($isList) $result.= "<th></th>";
                $flagfirst = false;
                $result .= "</tr></thead><tbody>";
            }
            $result .= '<tr>';
            foreach($row as $key => $value){
                $result .=  "<td>$value</td>"; 
            }
            if($isList) $result .= "<td><a class=\"link\" href=\"/$section/$row[Code]\">...</a></td>";
            $result .= "</tr>";
        }
        $result .= "</tbody></table>";
        return $result;
    }
    private function listView()
    {
        $table = self::FormatResult($this->data, $this->section);
       
        $this->template->assign('content', $table);
        $this->template->assign('pagination', $this->pagination->GetPagination());
        $this->template->display('index.tpl');
        

    }
    private function detailView()
    {
        $table = self::FormatResult($this->data, $this->section, false);
        $this->template->assign('content', $table);
        $this->template->assign('pagination', "<a class=\"link\" href=\"/$this->section/list/1\">Назад</a>");
        $this->template->display('index.tpl');
    }
    public function render()
    {
        $renderTpl = "{$this->viewType}View";
        $this->$renderTpl();
    }
}

