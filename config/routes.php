<?php 
    return array(
        '([a-z]+)/list/([0-9]+)' => 'content/list/$1/$2',
        '([a-z]+)/([0-9]+)' => 'content/detail/$1/$2',
        'signup' => 'user/signup',
        'auth' => 'user/auth',
        'logout' => 'user/exit',
        '' => 'content/list/nomenclature/1',
    );