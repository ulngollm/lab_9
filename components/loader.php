<?php
spl_autoload_register(array('Loader', 'autoload_component_class'));
spl_autoload_register(array('Loader', 'autoload_model_class'));
spl_autoload_register(array('Loader', 'autoload_view_class'));
spl_autoload_register(array('Loader', 'autoload_controller_class'));
class Loader{
    public static function autoload_component_class($class_name){
        $path = ROOT. "/components/$class_name.php";
        if(file_exists($path)){
            require_once($path);
        }
    }
    public static function autoload_model_class($class_name){
        $path = ROOT. "/models/$class_name.php";
        if(file_exists($path)){
            require_once($path);
        }
    }
    public static function autoload_view_class($class_name){
        $path = ROOT. "/views/$class_name.php";
        if(file_exists($path)){
            require_once($path);
        }
    }
    public static function autoload_controller_class($class_name){
        $path = ROOT. "/controllers/$class_name.php";
        if(file_exists($path)){
            require_once($path);
        }
    }
}