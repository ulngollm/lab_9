<?php
class Router
{

    private $routes;

    public function __construct()
    {
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);
    }
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }
    public function run()
    {
        $uri = $this->getURI();
        foreach ($this->routes as $pattern => $path) {
            if (preg_match("~$pattern~", $uri)) {

                $internalRoute = preg_replace("~$pattern~", $path, $uri);
                $segments = explode('/', $internalRoute);
                $controllerName = ucfirst(array_shift($segments)) . 'Controller';
                $actionName = 'action' . ucfirst(array_shift($segments));
                $parameters = $segments;

                $controllerFile = ROOT . "/controllers/$controllerName.php";
                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }
                $controller = new $controllerName;

                call_user_func_array(array($controller, $actionName), $parameters);
                break;
            }
        }
    }
}
